# matasano-crypto

[Matasano Crypto Challenges](https://cryptopals.com) solved by wangray

Working through matasano crypto challenges in python 2.7

Very thorough writeups are in each set's folder, and common classes/functions are in top-level directory.

[Set 1 writeup](https://github.com/wangray/matasano-crypto/blob/master/set1/set1_writeup.md)

[Set 2 writeup](https://github.com/wangray/matasano-crypto/blob/master/set2/set2_writeup.md)

[Set 3 writeup](https://github.com/wangray/matasano-crypto/blob/master/set3/set3-writeup.md)

[Set 4 writeup](https://github.com/wangray/matasano-crypto/blob/master/set4/set4-writeup.md)

Status: Halfway through set 4
